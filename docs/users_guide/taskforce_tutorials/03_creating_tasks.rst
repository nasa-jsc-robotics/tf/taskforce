.. _creating_tasks:

Tutorial: Creating Tasks
========================

The core computational element of TaskForce are called "Tasks".  To make a new Task, you simply need to subclass the Task
class.  Next, you will write a custom implementation for the pre-defined hooks.  Then, you will add your own custom
methods as needed, setup parameters, and define custom events.  TaskForce provides a default template to make creating
new Tasks fairly straight forward.  See the Task template
`here <https://gitlab.com/nasa-jsc-robotics/tf/taskforce_common/-/blob/master/src/taskforce_common/task/task_template.py>`_.

To create a Task from the template, we need to start up the Task Editor, if it is not currently running.  From the command
line run::

    taskforce

First, we need to decide where we want our new Task to live.  It needs to be placed in an existing library in the
library manager.  For this tutorial, we will create a new Task in the ``taskforce_example_library`` library.  For
instructions of how to add that library see :ref:`tutorial_setup_task_library`.

Let's start with creating a new folder called "tutorial".  In the Library Manager, expand ``taskforce_example_library``.
Right-click on ``taskforce_example_library`` and select "New" and then select "New Folder".  This will create a folder
called ``New Folder``.  Double-click on ``New Folder``, and change the name to ``tutorial``.

.. image:: create_folder.png
    :height: 400px
.. image:: new_folder_edit.gif
    :height: 400px
.. image:: tutorial_folder.gif
    :height: 400px

Now that we have a place for our new Task to live, we can create a new Task by using a built-in template.  Right-click
on the ``tutorial`` folder, and select "New Task (default template)".  This will pop up a dialog box to enter the name.
Here, we will name our Task "TutorialTask".

.. image:: create_task_default_template.gif
    :height: 400px
.. image:: create_task_dialog_box.gif
    :height: 400 px

When this is all done, we should now have a ``TutorialTask`` listed in our Library Manager, and the main view should
change to a text editor showing our template.  We can bring up the text editor at any time by double clicking our Task
from the Library Manager.

.. image:: tutorial_task_text_editor.gif
    :align: center

