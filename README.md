# TaskForce

This is the central hub for TaskForced relate repositories.

For the documentation and users guide, see:

[TaskForce Wiki](https://nasa-jsc-robotics.gitlab.io/tf/taskforce)

If you cannot access this page, instructions to build the documentation locally is [below](#build-documentation-locally).

## Description

TaskForce is a rapid prototyping framework for a component-based programming model.  
 
[TaskForce Overview](./docs/overview.md)


Although ROS is not necessarily required to use TaskForce, each repository is structured as a ROS catkin package.  This
has more to do with how code is managed internally, but lends itself well to a ROS workflow.  The following table is
a list of the core TaskForce repos and some optional plugins. 

| repo                                                                                              | description                                     |
|-------------------------------------------------------------------------------------------------- | ------------------------------------------------|
|[taskforce_common](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_common.git)                   | Common code library for the TaskForce frame work| 
|[taskforce_common_library](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_common_library.git)   | Common Tasks and Groups |
|[taskforce_engine](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_engine.git)                   | Code and executable for the Task Engine |
|[taskforce_example_library](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_example_library.git) | Collection of example Tasks and Groups for demonstration and testing |
|[taskforce_gui](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_gui.git)                         | Code an Executable for the Task Editor |
|[taskforce_library](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_library.git)                 | Code for managing TaskForce code libraries.  NOTE: this does NOT have interesting Task or groups in it |
|[taskforce_plugin_ros](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_plugin_ros.git)           | Extensions for using TaskForce with ROS | 
|[taskforce_plugin_ros_msgs](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_plugin_ros_msgs.git) | ROS-related messages for TaskForce |
|[taskforce_plugin_zmq](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_plugin_zmq.git)           | ZMQ plugin for TaskForce |
|[taskforce_view_library](https://gitlab.com/nasa-jsc-robotics/tf/taskforce_view_library.git)       | Library of views for the TaskEditor | 


## Create a ROS TaskForce Workspace

###### NOTE: This assumes that you have ROS kinetic already installed.  If you are using another distribution, adjust as necessary. ######

To download TaskForce, you will want to use `vcstool` to help with downloading the workspace.

	pip install vcstool

or 

	sudo apt install python-vcstool

First, create a new workspace folder:

	mkdir -p ~/taskforce_ws/src

Initialize your catkin workspace:

	cd ~/taskforce_ws
	catkin build
	catkin config --extend /opt/ros/kinetic

Next, download the workspace yaml file for the version of TaskForce you want to use (see the 
[workspaces folder](./workspaces)), and place it in your source folder (`~/taskforce_ws/src`).  The "numbered" releases 
(e.g. `taskforce_1.0.0.yaml`) point to a specific tag for each repo.  If you want to be on master for all the repos, you
can use `taskforce_ws.yaml`.  

Using vcstool, import the workspace.  This will clone all the repos you need into `~/taskforce_ws/src`

	cd ~/taskforce_ws/src
	vcs import --input <workspace file you downloaded>

###### NOTE: These workspaces may include other NASA-authored dependencies, outside this GitLab sub-group. ###### 

NASA JSC Robotics has some of it's own rosdep definitions.  So first, you need to download those rosdeps an update the
definitions.

    echo "yaml https://gitlab.com/nasa-jsc-robotics/nasa_common_rosdep/raw/master/nasa-pyqt5.yaml" | sudo tee /etc/ros/rosdep/sources.list.d/13-nasa-pyqt.list > /dev/null
    rosdep update
    
Now, we can install the dependencies.

    cd ~/taskforce_src
    rosdep install -i --from-path ./src
    
With all of the code downloaded and dependencies installed, build with:

	catkin build


## Build Documentation Locally

To build the documentation locally, you will need to install `sphinx`, `pygments`, `sphinx_rtd_theme`, and `m2r`.

	pip install --upgrade sphinx pygments sphinx_rtd_theme m2r

From this folder, build the documentation with Sphinx:

	sphinx-build -b html docs public

This will place all the HTML pages in `./public`.  Open `./public/index.html` with a web-browser.
