.. _tutorial_setup_task_library:

Tutorial: Setup Task Libraries
==============================

Groups and Tasks are stored in <i>libraries</i>.  A library can be of any arbitrary file structure, but is easier to 
manage when structured like a Python library.

First, we start the Task Editor from the command line. ::

    taskforce


You should see the main Task Editor pop up.

.. figure:: task_editor_blank.gif
    :alt: taskforce editor image
    :figclass: align-center

    TaskForce Editor

Adding a library
----------------

To add a new library, we are going to create a "link" to an existing folder on the file system.  Here, we will add
the ``taskforce_example_library``.

Right-click in the Library Manager, and choose "Add Link"

.. figure:: task_editor_add_link.gif
    :alt: task editor add link
    :figclass: align-center

    Add new library

From the file browser, navigate to find "taskforce_example_library" and click "Choose".

Once loaded, you should see "taskforce_example_library" in the Library Manager window.  If you expand the library, it
should look similar to the view below.

.. figure:: task_editor_tf_ex_lib.gif
    :alt: task editor taskforce example library
    :figclass: align-center

.. note::
    It is very common for task libraries to be contained within a ROS repository.  When adding these to the library,
    be sure to add the folder at the correct level.  For example, if the repo is structured list this:

    .. code-block:: bash

        /my_taskforce_library
            /src
                /my_taskforce_library
                    Task1.py
                    Group2.group

    You will want to link at the "second" level, i.e. ``.../my_taskforce_library/src/my_taskforce_library``

