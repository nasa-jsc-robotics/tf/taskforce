.. _tutorial_running_a_task:

Tutorial: Running a Task
=========================

.. note::  This tutorial assumes you have completed :ref:`tutorial_setup_task_library`

Starting up
***********

If the Task Editor is not currently running, start it up from the command line. ::

    taskforce

Before you can execute a Task, it first needs to be *deployed* to the Task Engine.  As we can see by the warning in the
Engine Status window, and the Log Window, the Task Engine is not connected.

Let's start the Task Engine from the command line in a new terminal. ::

    task_engine


.. note::
    When the Engine Status widget is yellow, it means that the user interface lost connection with the Task Engine.
    However, it should be noted that while creating or editing TaskForce Tasks and Groups, the engine is not necessary.
    We only need the engine to deploy and execute Tasks.

Once the engine is started, you should see the Engine Status turn white.

.. figure:: task_editor_engine_connected.gif
    :alt: taskforce editor image
    :figclass: align-center

Deploying a Group
*****************

Now, we can deploy a Group to the Task Engine.  A *Group* is a collection of Tasks.  You can think of it as being a
container to hierarchical structure to our code.

Expand the "taskforce_example_library" folder in the Library Manager, then expand "cow", and select "two_cows.group".

.. figure:: select_two_cows.gif
    :alt: taskforce editor image
    :figclass: align-center

    Select taskforce_example_library->cow->two_cows.group

Next, we right-click on our selection, and choose "Deploy"

.. figure:: choose_deploy.gif
    :alt: taskforce editor image
    :figclass: align-center

    Choose "Deploy"

You should see ``/two_cows`` pop up in the Engine Status window.  If you expand the ``/two_cows`` Group, you should see
a ``/clara`` and ``/clementine`` Task underneath.

.. figure:: two_cows_deployed.gif
    :alt: taskforce editor image
    :figclass: align-center

Now, we can execute our ``/two_cows`` Group from the Engine Status window.  But first, lets adjust the view in the
window to get some more information.

Right-click in the white space in the Engine Status window (just below the ``/clementine`` Task) and select "Adjust
Columns".  This will adjust the columns in the Engine Status table so you can see more information about each Task and
Group.  Next, double-left-click on the ``/two_cows`` Group.  It will present a "live" view of the ``/two_cows`` Group
in the main viewing area.  You may need to adjust you panel sizes to get everything to fit correctly on your monitor.

.. figure:: two_cows_live_view_start.gif
    :alt: taskforce editor image
    :figclass: align-center

    ``/two_cows`` live view


Starting a Group
****************

.. note::

    Before running these blocks, you must have ``cowsay`` installed on your system.  It can be installed with::

        sudo apt-get install cowsay

The picture in the main view window is called the Canvas.  It is showing a graphical representation of the ``/two_cows``
Group.  The two blue blocks, "clara" and "clementine" are Tasks.  Specifically, each one is an instance of the
``Cowtime`` Task.  The way these Blocks are connected, when we startup the ``/two_cows`` Group, it will trigger the
"startup" method of ``clara``.  When ``clara`` emits "complete", it will trigger the "startup" method of ``clementine``.
When ``clementine`` emits ``complete``, it will trigger the ``complete`` event for the entire Group.

To see this in action, right-click on the ``/two_cows`` Group in the Engine Status window, and select "startup".

.. figure:: two_cows_startup.gif
    :alt: taskforce editor image
    :figclass: align-center

    Executing the ``/two_cows`` Group

After the group finishes executing (which should be almost instantly), you should see the Tasks change color from blue
to green, and see the following in the terminal where you started the Task Engine.

.. figure:: two_cows_complete.gif
    :alt: two cows complete
    :figclass: align-center

    Task Editor after running ``/two_cows``


.. figure:: two_cows_terminal.png
    :alt: two cows terminal
    :figclass: align-center

    Terminal output after running ``/two_cows``


