TaskForce Overview
==================
The goal of TaskForce is to provide a rapid prototyping framework for a component-based programming model.  Users of
the framework will develop processing elements (called Tasks) in Python.  These Tasks can then be composed together with
other Tasks to form Groups.  Each Group can have not only Tasks, but other Groups as well.  Using this composition
pattern allows a developer to create a hierarchical application structured from these re-usable elements.  Tasks and
Groups can be connected together and pass Events.  In this fashion, a series of Tasks could be "chained" together to
execute in serial fashion, or even create a more complex web of interaction. 

Python was chosen as the language because of how quick and easy it is to develop (as compared to C++).  While it is easy
to argue a compiled language has better run-time performance, the goal of this platform is to optimize a 
"write some code -> run the code -> see what needs to be changed -> repeat" development cycle.  Python is a very common
and popular language which was another important consideration.

There are two primary components of TaskForce, the Task Engine, and the Task Editor.

Task Engine
-----------
The Task Engine is the heart of TaskForce.  It is the execution engine for TaskForce Tasks.  The Task Engine has a
well-defined API which allows Tasks and Groups to be started, stopped, loaded, unloaded, etc.  Once the engine is
running, users will typically use the Task Editor to deploy one or more a TaskForce Groups to the engine.  It is
important to note that the Task Engine can be run completely independent of the Task Editor.  Using the engine's API,
any other application can perform all actions that are performed in the Task Editor.


Task Editor
-----------
The task editor is a front end to aid in the development of high-level tasks.  This can be used for a robot, or any
application consisting of processing components.

The Task Editor in as an IDE for developing tasks. The goal was to create an easy-to-use, drag-and-drop interface to 
create and deploy applications.  With the Task Editor a user can:
 
* Create and edit TaskForce Tasks and Groups
* Deploy and execute code on the Task Engine
* Monitor the run-time status of your application
* Modify parameters at run-time
* Manage you libraries of Tasks and Groups

[Return to home](./README.md)
