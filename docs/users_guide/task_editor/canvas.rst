.. _canvas:

Canvas
======

Overview
--------
The Canvas is used to create Tasks and Blocks. Previously created Tasks and Blocks located in the Library Manager can be
easily dragged-and-dropped into the Canvas area and connected together to create a new Task or Block for the robot to
perform. Tasks are connected using an event system. Each Task can emit events and subscribe to events emitted by other
Tasks. By emitting and subscribing to events, a collection of Tasks and Blocks create a flow of control and execution
from one Task/Block to another. For example, a series of Tasks could be "chained" together to execute in serial fashion
where Task "A" triggers Task "B" which then triggers Task "C", etc...  However, a much more complex web of interaction
can be created by having Tasks subscribe to multiple events from multiple Tasks allowing the user to create a very
non-linear execution flow if desired.

Creating a new Task or Group
----------------------------

To create a new Task or Block, review the :ref:`library_manager` section of this User's Guide. Creating a
new Task from scratch requires the ability to code using Python.

Using an Existing Task or Block
-------------------------------

