.. _install_instructions:

Install instructions
====================

Environment
-----------

TaskForce was developed in Ubuntu 16.04 using Python 2.7.  While TaskForce might work in
other environments, they are not supported.

TaskForce was designed as to not directly depend on ROS.  However, for internal development reasons, the TaskForce code 
repositories are structured as ROS Python packages, and leverage the ROS catkin build system to build the code.  Most
testing has been done in `ROS Kinetic <http://wiki.ros.org/kinetic>`_.

Because TaskForce is broken up into several different repositories, we use `vcstool` to manage the workspace of
repositories.

Prerequistites
--------------

Some of these may already be installed on your system.  If not, here's how to get them:

python pip - python package management tool::

    sudo apt install python-pip
    
catkin-tools - extra build tools for ROS::

    pip install catkin-tools
    
vcstool - workspace management tool::

    pip install vcstool

Create a workspace
------------------

First, you'll want a place to put the code.  Assuming you aren't using another workspace already.::

    mkdir -p ~/taskforce_ws/src

Next, initialize the catkin workspace, extending the ROS kinetic workspace explicitly.::

    cd ~/taskforce_ws
    catkin config --init
    catkin config --extend /opt/ros/kinetic

.. note::

    You can extend any other workspace by changing ``/opt/ros/kinetic`` above to any other workspace you like.

Get the workspace file, and put it in the ``taskforce_ws/src`` folder.  In the main TaskForce documentation repository
(which includes this documentation), the
`workspaces <https://gitlab.com/nasa-jsc-robotics/tf/taskforce/-/tree/master/workspaces>`_ folder has a set of
workspaces to use for various versions.

Import the code::
    
    cd ~/taskforce_ws/src
    vcs import --input taskforce_ws.yaml

Install the dependencies
------------------------

We have some custom rosdep definitions.  They will need to be installed along with the rest of your rosdeps.::
        
    echo "yaml https://gitlab.com/nasa-jsc-robotics/nasa_common_rosdep/raw/master/nasa-common.yaml" >> /etc/ros/rosdep/sources.list.d/10-nasa-common.list
    echo "yaml https://gitlab.com/nasa-jsc-robotics/nasa_common_rosdep/raw/master/nasa-pyqt5.yaml" >> /etc/ros/rosdep/sources.list.d/13-nasa-pyqt.list

In order to manage the dependencies, we rely on some custom rosdep definitions.::

    cd ~/taskforce_ws
    rosdep install -i --from-paths ./src


Build the Code
--------------

Now, we should be able to build the code.::

    cd ~/taskforce_ws
    catkin build

Once the code is built, you can add the following to your ``~/.bashrc``::

    source ~/taskforce_ws/devel/setup.bash`
    
Return to :ref:`taskforce_users_guide`.
