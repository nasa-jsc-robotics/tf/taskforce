.. _task_editor:

Task Editor Guide
=================

The Task Editor is a graphical user interface that can be used to create TaskForce applications and interact with those 
applications at run-time. Below is an example of the Graphical User Interface (GUI) with the major sections labeled.

.. figure:: TaskForce11.jpg
    :alt: taskforce editor image
    :figclass: align-center

    TaskForce Editor panes

.. toctree::
    :maxdepth: 2

    canvas
    engine_library
    engine_status
    library_manager
    log_window
    parameters
